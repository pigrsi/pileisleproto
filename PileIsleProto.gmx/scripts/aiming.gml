if (aim_start_x == -1 && aim_start_y == -1) {
    // initialize starting point of aim
    aim_start_x = mouse_x;
    aim_start_y = mouse_y;
}

var max_mag = 120;
var dx = aim_start_x - mouse_x;
var dy = aim_start_y - mouse_y;
var mag = sqrt(dx*dx + dy*dy);
var unit_dx = dx / mag;
var unit_dy = dy / mag;
// If mag is too big, set to max for that unit vector
if (mag > max_mag) {
    dx = unit_dx * max_mag;
    dy = unit_dy * max_mag;
}
fireX = dx/8;
fireY = dy/8;
