if (player_state == states.idle) {
    var offscreen = false;
    with (players[turn_player])
        offscreen = script_execute(is_offscreen);
    if (offscreen) {
        player_moved = false; // when going back to idle
        turn_player = (turn_player + 1) % num_players;
        show_debug_message("Player " + string(turn_player) + "'s turn");
        exit;  
    }
    script_execute(all_rock_pickup);
    if (!player_moved && keyboard_check(ord('Z'))) {
        player_moved = true;
        player_state = states.about_to_move;
        show_debug_message("Player " + string(turn_player) + " about to move.");
    } else if (players[turn_player].holding != noone && keyboard_check(ord('X'))) {
        player_state = states.about_to_throw;
        show_debug_message("Player " + string(turn_player) + " about to throw.");
    } else if (player_moved && (keyboard_check(ord('C')) || players[turn_player].holding == noone)) {
        // skip throw
        player_moved = false; // when going back to idle
        turn_player = (turn_player + 1) % num_players;
        show_debug_message("Player " + string(turn_player) + "'s turn");
    }
} else if (player_state == states.about_to_move) {
    if (mouse_check_button_pressed(mb_left)) {
        player_state = states.move_dragging;
    }
} else if (player_state == states.about_to_throw) {
    if (mouse_check_button_pressed(mb_left)) {
        player_state = states.throw_dragging;
    }
} else if (player_state == states.move_dragging) {
    script_execute(aiming)
    if (mouse_check_button_released(mb_left)) {
        player_state = states.moving;
        players[turn_player].hspeed = fireX;
        players[turn_player].vspeed = fireY;
        players[turn_player].gravity = 0.25;
        var holding = players[turn_player].parent;
        with (holding)
            script_execute(stop_holding);
        players[turn_player].parent = noone;
        with(players[turn_player]) {
            if (hspeed > 0)
                sprite_index = spr_player;
            else
                sprite_index = spr_player2;
        }
            
            //object_set_sprite(1, spr_player2);
        show_debug_message("Player " + string(turn_player) + " moving");
        aim_start_x = -1;
        aim_start_y = -1;
    }
} else if (player_state == states.moving) {
    // Move on when player settles
    if (mouse_check_button_pressed(mb_right)) {
        players[turn_player].x = mouse_x;
        players[turn_player].y = mouse_y;
        players[turn_player].hspeed = 0;
        players[turn_player].vspeed = 0.1;
    }
    var offscreen = false;
    with (players[turn_player])
        offscreen = script_execute(is_offscreen);
    if ((players[turn_player].hspeed == 0 && players[turn_player].vspeed == 0) || offscreen) {    
        player_state = states.idle;
        show_debug_message("Player " + string(turn_player) + " has finished moving");
        with (players[turn_player])
            script_execute(player_pickup);
    }
} else if (player_state == states.throw_dragging) {
    script_execute(aiming)
    if (mouse_check_button_released(mb_left)) {
        player_state = states.throwing;
        show_debug_message("Player " + string(turn_player) + " throwing.");
        aim_start_x = -1;
        aim_start_y = -1;
    }
} else if (player_state == states.throwing) {
    // Move on when everything settles
    var holding = players[turn_player].holding;
    if (holding != noone) {
        holding.hspeed = fireX;
        holding.vspeed = fireY;
        holding.gravity = 0.25;
        if (holding.holding != noone) {
            holding.holding.gravity = 0.25;
            with (holding)
                script_execute(stop_holding);
        }
        with (players[turn_player])
            script_execute(stop_holding);
    }
    player_state = states.idle;
    player_moved = false; // when going back to idle
    turn_player = (turn_player + 1) % num_players;
    show_debug_message("Player " + string(turn_player) + "'s turn");
}
