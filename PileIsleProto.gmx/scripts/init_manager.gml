var i;
for (i = 0; i < instance_number(obj_player); i += 1) {
    players[i] = instance_find(obj_player,i);
}
num_players = i

turn_player = 0;
enum states {
    idle,
    about_to_move,
    move_dragging,
    moving,
    about_to_throw,
    throw_dragging,
    throwing
}
player_state = states.idle;
player_moved = false;
fireX = 0;
fireY = 0;
aim_start_x = -1;
aim_start_y = -1;
show_debug_message("Player " + string(turn_player) + "'s turn");
